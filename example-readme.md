# Three Word Title
`Codename: <Example>`  
[`Current Version: 0`](https://gitlab.com/NuclearEagleFox/example/tags/v0)

A short description. Built using <language> and <library>.

### Building & Running
```
<example-commands>
```

### Status & Contribution
At this time, I consider this project active. However, I am developing it for my own personal use. Forks are therefore preferred to pull requests.

### Roadmap
A guide to planned features and future versions.

##### Version 1
* Example feature

### Related Project(s)
<http://www.zombo.com>