# Command-line Project Initializer
`Codename: Trilobite`  
[`Current Version: 2`](https://gitlab.com/NuclearEagleFox/trilobite/tags/v2)

Initializes new projects and fits over old ones. Built using Batch and Windows Command Prompt.

### Building & Running
```
project-init example-codename example-foldername
```

### Status & Contribution
At this time, I consider this project active. However, I am developing it for my own personal use. Forks are therefore preferred to pull requests.

### Roadmap
A guide to planned features and future versions.

There are currently no planned features.

### Related Project(s)
https://gitlab.com/NuclearEagleFox/stork