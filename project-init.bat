@echo off

set projectInitFolder=C:\Users\NuclearEagleFox\Software\project-init
set gitlabUsername=nucleareaglefox

echo Project Initializer Version 2
echo.

if "%1" == "--story" (

    set /a story=1
    
    if "%3" == "" (
    
        set codeName=%2
        set folderName=%2
    
    ) else (
    
        set codeName=%2
        set folderName=%3
    
    )

) else (

    set /a story=0
    
    if "%2" == "" (
    
        set codeName=%1
        set folderName=%1
    
    ) else (
    
        set codeName=%1
        set folderName=%2
    
    )

)

if "%codeName%" == "" (

    echo Please provide a code name.
	exit /b

)

if not exist %folderName% (

    mkdir %folderName%
    echo Directory created.

) else (

    echo Directory exists.

)

cd %folderName%

git status 1>NUL 2>NUL && (

    git init 1>NUL 2>NUL
    echo Git repo created.

) || (

    echo Git repo exists.

)

git remote add gitlab https://gitlab.com/%gitlabUsername%/%codeName%.git 1>NUL 2>NUL
echo Remote repo updated.

if exist "README.md" (

    echo README exists.

) else (

    if %story% == 1 (
    
        copy %projectInitFolder%\example-story-readme.md README.md 1>NUL 2>NUL
        
    ) else (

        copy %projectInitFolder%\example-readme.md README.md 1>NUL 2>NUL
    
    )
    echo README created.

)

if exist ".gitignore" (

    echo Gitignore exists.

) else (

    copy %projectInitFolder%\.example-gitignore .gitignore 1>NUL 2>NUL
    echo Gitignore created.

)

git add . 1>NUL 2>NUL
echo Changes staged.

git commit -m "Initial commit" 1>NUL 2>NUL
echo Changes committed.

git push gitlab master 1>NUL 2>NUL
echo Changes pushed.

echo Mission complete!